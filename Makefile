# Makefile для сборки ВКР.
# Автор: Шмаков И.А.
# Версия: 08.06.2021

default: help
.PHONY: default

report:
	xelatex -shell-escape -8bit main-report-csae && biber main-report-csae && xelatex -shell-escape -8bit main-report-csae && make clean
.PHONY: book

clean:
	rm *.aux *.log *.bbl *.bcf *.blg *.toc *.run.xml *.out && rm -rf _minted-main-report-csae/
.PHONY: clean

# Help Target
help:
	@echo "Следующие цели представлены в данном Makefile:"
	@echo "... help (Данная цель является целью по умолчанию)"
	@echo "... book сборка книги"
	@echo "... clean удаление временных файлов"
.PHONY: help
